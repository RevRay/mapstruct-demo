package ru.devray.mapstruct.demo.mapper;

import ru.devray.mapstruct.demo.dto.Tea;
import ru.devray.mapstruct.demo.dto.ShopTea;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface MapStructTeaMapper {

    @Mapping(source = "teaName", target = "newTeaName")
    Tea shopTeaToTea(ShopTea shopTea);

}
