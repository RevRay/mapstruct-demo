package ru.devray.mapstruct.demo.mapper;

import ru.devray.mapstruct.demo.dto.Tea;
import ru.devray.mapstruct.demo.dto.ShopTea;

public class OldTeaMapper {

    //так пришлось бы поступать без MapStruct
    //реализацию маппера с MapStruct смотри в классе MapStructTeaMapper
    Tea shopTeaToTea(ShopTea shopTea) {
        Tea tea = new Tea();
        tea.setNewTeaName(shopTea.getTeaName());
        tea.setType(shopTea.getType());
        return tea;
    }

}
