package ru.devray.mapstruct.demo;

import org.mapstruct.factory.Mappers;
import ru.devray.mapstruct.demo.dto.Tea;
import ru.devray.mapstruct.demo.dto.ShopTea;
import ru.devray.mapstruct.demo.mapper.MapStructTeaMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MapStructTest {

    @Test
    public void test() {
        //1. объект первого типа
        Tea newTea = new Tea();
        newTea.setNewTeaName("Жоу Гуй");
        newTea.setType("улун");

        //2. объект второго типа
        ShopTea teaFromCatalog = new ShopTea();
        teaFromCatalog.setId(100);
        teaFromCatalog.setTeaName("Жоу Гуй");
        teaFromCatalog.setType("улун");

        //3. маппер, который превратит объект второго типа в объект первого типа
        MapStructTeaMapper mapper = Mappers.getMapper(MapStructTeaMapper.class);

        Tea convertedShopTea = mapper.shopTeaToTea(teaFromCatalog);
        Assert.assertEquals(newTea, convertedShopTea);
    }

}
