package ru.devray.mapstruct.demo.dto;

import lombok.Data;

@Data
public class Tea {
	private String newTeaName;
	private String type;
}