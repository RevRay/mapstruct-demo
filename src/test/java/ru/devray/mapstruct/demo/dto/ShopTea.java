package ru.devray.mapstruct.demo.dto;

import lombok.Data;

@Data
public class ShopTea {
	private int id;
	private String teaName;
	private String type;
}